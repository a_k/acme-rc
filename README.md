acme interfaces for various applications written in rc
============

Several interfaces are developed for various types of applications (i.e., IRC, dictionaries), for use in the *acme* editor in Plan 9 Ports and Plan 9 proper, and written in *rc*. This is made possible by the *shell script support* provided by [*acmeevent*(1)](http://swtch.com/plan9port/man/man1/acmeevent.html) from Plan 9 Ports (and a slight modification for Plan 9, provided in the repo).

### Preliminaries ###

Acme is a text editor, originally written for Plan 9, but now available on many platforms thanks to [Plan 9 Ports](http://swtch.com/plan9port/). For more information, check out Russ Cox's "[A Tour of Acme](http://research.swtch.com/acme)."

Rc is the main shell for Plan 9 and is also part of Plan 9 Ports. It is the preferred scripting language for many Plan 9 users, as some of its features allow a very succinct writing style when it comes to process management (distributed executions, parent-child relations), pipelining, and the like. See Tom Duff's write-up, "[Rc -- The Plan 9 Shell](http://plan9.bell-labs.com/sys/doc/rc.html)" for more information. 

### Applications ###

**airc**: IRC interface to Mechiel's (mjl) [ircfs](https://bitbucket.org/mjl/ircfs). The backend filesystem requires Inferno and needs to be mounted locally. The fs can also be imported using Plan 9 Ports with the [*import*(4)](http://swtch.com/plan9port/man/man4/import.html) tool, in which case one can also use [*aan*(8)](http://plan9.bell-labs.com/magic/man2html/8/aan) for stability (*aan* is available through a patch available on [codereview](https://codereview.appspot.com/8192043/)).

**nadict**: *adict* from Plan 9, slightly modified to dedup open entries/windows.

